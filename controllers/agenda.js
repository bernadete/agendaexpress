const express = require('express');

const Agenda = require('../models/agenda');

const router = express.Router();

router.post('/agendamento', async (req, res) => {
    try {

        const agenda = await Agenda.create(req.body);
        return res.send({
            agenda
        });

    } catch (err) {
        return res.status(400).send({
            error: 'Compromisso não foi criado!'
        });
    }

});

module.exports = app => app.use('/agenda', router);
const mongoose = require('../database');

const AgendaSchema = new mongoose.Schema({

    
    compromisso: {
        type: String,
        required: true,
    },
    observacao: {
        type: String,
        required: false,
    },
    data: {
        type: Date,
        default: Date.now,
    }
});

const Agenda = mongoose.model('Agenda', AgendaSchema);

module.exports = Agenda;